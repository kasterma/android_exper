package net.kasterma.android_exper;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class Android_experActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        TextView TV = (TextView) findViewById(R.id.helloText);
        
        // the following gives java.lang.OutOfMemoryError
//        int i;
//        int [] arr;
//        for (i = 10000; i < 10000000; i+= 10000) {
//        	arr = new int [i];
//        	TV.setText("i is now: " + i);
//        	Log.v("OOM", "i is now: " + i);
//        }
        
        // the following just keeps running with no visible output
        // then when pressing a button it gave me ANR (application not responding)
        int i;
        for (i = 0; i < 9000000; i++) {
        	TV.setText("i is now: " + i);  // not much point as we keep the interface thread busy
        }
    }
}